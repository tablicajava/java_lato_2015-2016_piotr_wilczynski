package tablica;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ChatServ implements Runnable{
    private static final int PORT = 8081;
    
    public static HashSet<String> names = new HashSet<String>();
    
    public static HashSet<PrintWriter> writers = new HashSet<PrintWriter>();
    
    public static HashSet<String> msgbase = new HashSet<String>();
    
    SimpleDateFormat simpleDateHere = new SimpleDateFormat("yyyy-MM-dd kk:mm:ss");
    static int iB = 0;
    static String msgb = "";
    
    public static void starts() throws IOException
    {        
        System.out.println("Server chatu jest uruchomiony.");
        ServerSocket listener = new ServerSocket(PORT);
        try {
            while (true) {
                new ChatServ.Handler(listener.accept()).start();               
            }
        } finally {
            listener.close();
        }
    }

    @Override
    public void run(){
        try {
            starts();
        } catch (IOException ex) {
            Logger.getLogger(ChatServ.class.getName()).log(Level.SEVERE, null, ex);
        }
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    public static class MsgBase{
        public int Id;
        public String name;
        public String message;
        //System.out.println( simpleDateHere.format(new Date()) );
        public String date; //= simpleDateHere.format(new Date());
    }
    
    private static class Handler extends Thread {
        private String name;
        private Socket socket;
        private BufferedReader in;
        private PrintWriter out;
        
        
        public Handler(Socket socket) {
            this.socket = socket;
        }

        public void run() {
            try {
                in = new BufferedReader(new InputStreamReader(
                    socket.getInputStream()));
                out = new PrintWriter(socket.getOutputStream(), true);

                while (true) {
                    out.println("SUBMITNAME");
                    name = in.readLine();
                    if (name == null) {
                        return;
                    }
                    synchronized (names) {
                        if (!names.contains(name)) {
                            names.add(name);
                            break;
                        }
                    }
                }
                                
                out.println("NAMEACCEPTED");
                writers.add(out);
                
                writers.stream().forEach((writer) -> {
                    Iterator it = names.iterator();
                    
                    while(it.hasNext())
                        writer.println("LISTA " + it.next());
                });
                
                SimpleDateFormat simpleDateHere = new SimpleDateFormat("yyyy-MM-dd kk:mm:ss");
                
                while (true) {
                    String input = in.readLine();
                    if (input == null) {
                        System.out.println("nic");
                        return;
                    }
                    
                    if (input.startsWith("DRAW")){
                        for (PrintWriter writer : writers) {
                            writer.println("DRAW " + input);
                            System.out.println(input);
                        }
                    }else if(input.startsWith("MESSAGE")){
                        if(input.contains("!TIME")){
                            for (PrintWriter writer : writers) {
                            writer.println("MESSAGE " + name + ": " + simpleDateHere.format(new Date()));
                        }
                        }else{
                            for (PrintWriter writer : writers) {
                                writer.println("MESSAGE " + name + ": " + input.substring(7));
                                msgb=iB+"::"+name+"::"+input.substring(7)+"::"+simpleDateHere.format(new Date());
                                msgbase.add(msgb);
                                iB = iB+1;
                                msgb="";
                            }
                        }
                    }else if(input.startsWith("CLEARSCREEN")){
                        for (PrintWriter writer : writers) {
                            writer.println("CLEARSCREEN");
                        }
                    }else if(input.startsWith("CLEARCHAT")){
                        for (PrintWriter writer : writers) {
                            writer.println("CLEARCHAT");
                        }
                    }else if(input.startsWith("SAVE")){
                        for (PrintWriter writer : writers) {
                            Iterator save = msgbase.iterator();
                            while(save.hasNext()) writer.println("SAVE " + save.next());
                        }
                    }else if(input.startsWith("MSGDEL")){
                        for (PrintWriter writer : writers) {
                            writer.println("MSGDEL");
                        }
                    }
                }
            } catch (IOException e) {
                System.out.println(e);
            } finally {
                if (name != null) {
                    
                    writers.stream().forEach((writer) -> {
                        writer.println("EXIT " + "Użytkownik " + name + " opuścił konwersacje... \n");
                        writer.println("EXIUSER " + name);
                    });
                    names.remove(name);
                }
                if (out != null) {
                    writers.remove(out);
                }
                try {
                    socket.close();
                } catch (IOException e) {
                }
            }
        }
    }
}
